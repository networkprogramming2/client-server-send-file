using System;
using System.IO;
using System.Net.Sockets;

namespace ClientExample
{
    class Program
    {
        static void Main(string[] args)
        {
            string serverIp = "127.0.0.1";
            int serverPort = 25697;

            using (TcpClient client = new TcpClient())
            {
                client.Connect(serverIp, serverPort);
                Console.WriteLine("Connected to server!");
                Console.WriteLine("Read from socket");

                using (NetworkStream stream = client.GetStream())
                {
                    byte[] buffer = new byte[1024];
                    int bytesRead;

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            Console.WriteLine("Write into buffer and");
                            memoryStream.Write(buffer, 0, bytesRead);
                        }

                        byte[] fileData = memoryStream.ToArray();
                        File.WriteAllBytes("received_a.txt", fileData);
                    }

                    Console.WriteLine("File received and saved as 'received_a.txt'.");
                }
            }
        }
    }
}
