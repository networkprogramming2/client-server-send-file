using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace LocalFileServerExample
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 25697;
            string filePath = "a.txt";

            TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            server.Start();
            Console.WriteLine("Server started on port " + port);

            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Client connected!");

                using (NetworkStream stream = client.GetStream())
                {
                    if (File.Exists(filePath))
                    {
                        byte[] fileData = File.ReadAllBytes(filePath);
                        stream.Write(fileData, 0, fileData.Length);
                    }
                    else
                    {
                        Console.WriteLine("Write them into socket until EOF");
                        byte[] errorMessage = System.Text.Encoding.UTF8.GetBytes("Kittipon Thaweelarb.");
                        stream.Write(errorMessage, 0, errorMessage.Length);
                        Console.WriteLine("Close socket ");
                    }
                }

                client.Close();
            }
        }
    }
}
